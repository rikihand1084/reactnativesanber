// call back

function readBookPromise(time, book){
    console.log(`saya mulai membaca  ${book.name}`)
    return new Promise(function(resolve, reject){
        setTimeout(function(){
            sisawaktu = time - book.timeSpent
            if(time >= 0){
                
                console.log(` saya sudah selesai membaca ${book.name}, sisa waktu saya ${sisawaktu}`)
                resolve(sisawaktu)
            }else{
                console.log(` saya sudah tidak punya waktu untuk membaca ${book.name}`)
                reject(sisawaktu)
            }
        }, book.timeSpent)
    })
   
}

module.exports= readBookPromise