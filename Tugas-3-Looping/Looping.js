var tugas1 = 1;
console.log("LOOPING PERTAMA")
while(tugas1 <= 10) { 
    console.log(tugas1*2 + ' - I love coding'); 
    tugas1++; 
}

var tugas2 = 10;
console.log("LOOPING KEDUA")
while(tugas2 >= 1) { 
    console.log(tugas2*2 + ' - I will become a mobile developer'); 
    tugas2--; 
}


console.log(' ')

for (var x = 1; x < 21; x++) {
    if(x % 2 == 0){
        console.log(x + ' - ' + 'Berkualitas')
    }else if(x % 3 == 0) {
        console.log(x + ' - ' + 'I Love Coding')
    }else if(x % 2 !== 0) {
        console.log(x + ' - ' + 'Santai')
    }
}

console.log(' ')

var Persegi = 1
while(Persegi < 5) {
    Persegi++;
    console.log('########')
}

console.log(' ')

var hasil = ''; {
    for (var i = 1; i < 8; i++) {
        for (var j = 1; j <= i; j++) {
            hasil += '#';
        }
        hasil += '\n';
    }
    console.log(hasil);
}

console.log(' ')

var hasil = ''; {
    for (var i = 1; i < 9; i++) {
        if(i % 2 == 0) {
            console.log(' # # # #')
        }else{
            console.log('# # # # ')
        }
        
        hasil += '\n';
    }
    console.log(hasil);
}

console.log(' ')